set(FLAGS_LIST
        -Wall
        -Wextra
        -Wpedantic
        -Wcast-align
        -Wcast-qual
        -Wdouble-promotion
        -Wformat=2
        -Winit-self
        -Winvalid-pch
        -Wlogical-op
        -Wmissing-declarations
        -Wmissing-include-dirs
        -Wnoexcept
        -Woverloaded-virtual
        -Wredundant-decls
        -Wshadow
        -Wsign-conversion
        -Wsign-promo
        -Wstrict-null-sentinel
        -Wstrict-overflow=1
        -Wtrampolines
        -Wundef
        -Wunsafe-loop-optimizations
        -Wvector-operation-performance
        -Wzero-as-null-pointer-constant
        -Werror
        -O3
        -Wdeprecated-declarations
        )

string(REPLACE ";" " " FLAGS_STRING "${FLAGS_LIST}")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FLAGS_STRING}")