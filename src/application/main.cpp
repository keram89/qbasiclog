#include <QApplication>

#include "MainWindow.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("QBasicLog");

    auto* mainWindow = new widgets::MainWindow();

    mainWindow->show();
    int result = QApplication::exec();

    delete mainWindow;
    return result;
}