#include "MainWindow.h"

#include "FilterData.h"
#include "ILoadData.h"
#include "LoadDataSingleChunk.h"
#include "ReverseFilterProxyModel.h"

#include <filesystem>

#include <QAbstractItemModel>
#include <QAbstractItemView>
#include <QAction>
#include <QCheckBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QFont>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QKeySequence>
#include <QLabel>
#include <QLineEdit>
#include <QMenuBar>
#include <QMessageBox>
#include <QSplitter>
#include <QStandardPaths>
#include <QTableView>
#include <qnamespace.h>
#include <qtabwidget.h>

namespace widgets {
constexpr int modelTabShifter = 1;

MainWindow::MainWindow()
    : QMainWindow(nullptr)
    , mMainTextView(new QTableView(this))
    , mFilterTextView(nullptr)
    , mLogsModel(new QStandardItemModel(0, 2, this))
    , mFilterTextViewModel(new QSortFilterProxyModel(this))
    , mTextFilterRegex(false)
    , mTextFilterCaseSensitive(false)
    , mEmptyItemModel(new QStandardItemModel(0, 2, this))
{
    mCentralWidget = new QWidget();
    mMapModels[1] = mLogsModel;

    /* Menu Bar - Begin*/
    QMenuBar *menuBar = new QMenuBar(this);

    QMenu *fileMenu = menuBar->addMenu(tr("&File"));

    QAction *openAction = new QAction(tr("&Open"), this);
    connect(openAction, &QAction::triggered, this, &MainWindow::openFile);
    fileMenu->addAction(openAction);
    QAction *saveProjectSettings = new QAction(tr("&Save Project"), this);
    fileMenu->addAction(saveProjectSettings);

    QMenu *editMenu = menuBar->addMenu(tr("&Edit"));

    QAction *copyAction = new QAction(tr("&Copy"), this);
    editMenu->addAction(copyAction);
    QAction *selectAllAction = new QAction(tr("&Select All"), this);
    editMenu->addAction(selectAllAction);

    QMenu *viewMenu = menuBar->addMenu(tr("&View"));

    QAction *reloadAction = new QAction(tr("&Reload"), this);
    viewMenu->addAction(reloadAction);
    QAction *followFileAction = new QAction(tr("&Follow File"), this);
    viewMenu->addAction(followFileAction);
    QAction *clearFilterAction = new QAction(tr("&Clear filter view"), this);
    clearFilterAction->setShortcut(QKeySequence(Qt::Key_F10));
    connect(clearFilterAction, &QAction::triggered, this, &MainWindow::clearMainFilterText);
    viewMenu->addAction(clearFilterAction);

    QMenu *toolsMenu = menuBar->addMenu(tr("&Tools"));

    QAction *filtersAction = new QAction(tr("&Filters"), this);
    toolsMenu->addAction(filtersAction);
    QAction *settingsAction = new QAction(tr("&Settings"), this);
    toolsMenu->addAction(settingsAction);

    QMenu *encodingMenu = menuBar->addMenu(tr("&Encoding"));

    QAction *autoEncodingAction = new QAction(tr("&Auto"), this);
    encodingMenu->addAction(autoEncodingAction);

    QMenu *helpMenu = menuBar->addMenu(tr("&Help"));

    QAction *aboutAction = new QAction(tr("&About"), this);
    helpMenu->addAction(aboutAction);

    setMenuBar(menuBar);
    /* Menu Bar - End*/

    QVBoxLayout *mainLayout = new QVBoxLayout(mCentralWidget);

    /* Reverse filter - Begin */
    QHBoxLayout *reverseFilterLayout = new QHBoxLayout();
    QLabel *reverseFilterLabel = new QLabel("Reverse:");

    QLineEdit *reverseFilterEdit = new QLineEdit();
    connect(reverseFilterEdit, &QLineEdit::returnPressed, this, [=]() {
        filtrReverseMainView(reverseFilterEdit->text());
    });

    QCheckBox *ignoreCaseReverseFilter = new QCheckBox("Case sensitivity");
    connect(ignoreCaseReverseFilter, &QCheckBox::stateChanged, this, [=](int state) {
        mReverseTextFilterCaseSensitive = state ? true : false;
    });
    QCheckBox *regexReverseFilter = new QCheckBox("regex");
    connect(regexReverseFilter, &QCheckBox::stateChanged, this, [=](int state) {
        mReverseTextFilterRegex = state ? true : false;
    });
    reverseFilterLayout->addWidget(reverseFilterLabel);
    reverseFilterLayout->addWidget(reverseFilterEdit);
    reverseFilterLayout->addWidget(ignoreCaseReverseFilter);
    reverseFilterLayout->addWidget(regexReverseFilter);
    mainLayout->addLayout(reverseFilterLayout);
    /* Reverse filter - End */

    /* Tabs with file text - Begin */
    mMainTextTab = new QTabWidget(this);
    mMainTextTab->setTabsClosable(true);
    connect(mMainTextTab, &QTabWidget::tabCloseRequested, this, &MainWindow::closeTab);

    mMainTextView->horizontalHeader()->hide();
    mMainTextView->verticalHeader()->hide();
    mMainTextView->setParent(mMainTextTab);
    mMainTextView->setModel(mLogsModel);
    mMainTextView->setItemDelegateForColumn(0, &mGreyDelegate);
    mMainTextView->setFont(QFont("Times", fontSize));
    mMainTextView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mMainTextView->setShowGrid(false);
    mLogsModel->setParent(mMainTextTab);
    QWidget *clearTextWidget = new QWidget(mMainTextTab);
    QVBoxLayout *widgetLayout = new QVBoxLayout(mMainTextTab);
    widgetLayout->addWidget(mMainTextView);
    clearTextWidget->setLayout(widgetLayout);
    mMainTextTab->addTab(clearTextWidget, "Empty");
    /* Tabs with file text - End */

    /* Table with filter text - Begin */
    mFilterTextView = new QTableView(mCentralWidget);
    mFilterTextView->setMinimumHeight(100);
    mFilterTextView->horizontalHeader()->hide();
    mFilterTextView->verticalHeader()->hide();
    mFilterTextView->setItemDelegateForColumn(0, &this->mGreyDelegate);
    mFilterTextView->setFont(QFont("Times", fontSize));
    mFilterTextView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mFilterTextView->setShowGrid(false);
    mFilterTextView->verticalHeader()->setMinimumSectionSize(sectionSize);
    mFilterTextView->verticalHeader()->setMaximumSectionSize(sectionSize);
    mFilterTextView->verticalHeader()->setDefaultSectionSize(sectionSize);
    QSplitter *splitter = new QSplitter(Qt::Vertical, mCentralWidget);
    splitter->setMinimumSize(QSize(1, 1));
    splitter->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    splitter->addWidget(mMainTextTab);
    splitter->addWidget(mFilterTextView);
    mainLayout->addWidget(splitter);
    splitter->setStretchFactor(0, 1);

    /* Table with filter text - End */

    /* Text filter - Begin */
    QHBoxLayout *textFilterLayout = new QHBoxLayout();
    QLabel *textFilterLabel = new QLabel("Text:");

    QLineEdit *textFilterEdit = new QLineEdit();
    connect(textFilterEdit, &QLineEdit::returnPressed, this, [=]() {
        filtrMainView(textFilterEdit->text());
    });

    QCheckBox *textCaseTextFilter = new QCheckBox("Case sensitivity");
    connect(textCaseTextFilter, &QCheckBox::stateChanged, this, [=](int state) {
        mTextFilterCaseSensitive = state ? true : false;
    });

    QCheckBox *regexTextFilter = new QCheckBox("Regex");
    connect(regexTextFilter, &QCheckBox::stateChanged, this, [=](int state) {
        mTextFilterRegex = state ? true : false;
    });

    textFilterLayout->addWidget(textFilterLabel);
    textFilterLayout->addWidget(textFilterEdit);
    textFilterLayout->addWidget(textCaseTextFilter);
    textFilterLayout->addWidget(regexTextFilter);
    mainLayout->addLayout(textFilterLayout);
    /* Text filter - End */

    mCentralWidget->setLayout(mainLayout);
    this->setCentralWidget(mCentralWidget);

    this->setMinimumHeight(600);
    this->setMinimumWidth(800);
}

void MainWindow::openFile()
{
    QString homeDir = QStandardPaths::writableLocation(QStandardPaths::HomeLocation);
    QString filePath = QFileDialog::getOpenFileName(this, tr("Open Logs"), homeDir);
    if (filePath.isEmpty()) {
        return;
    }
    mLogsModel->clear();
    fileoperations::LoadDataSingleChunk load;
    try {
        load.loadDataToModel(mLogsModel, filePath);
    } catch (std::filesystem::filesystem_error const &ex) {
        QMessageBox msgBox;
        msgBox.setText("Error during file opening, check your file.");
        msgBox.exec();
        return;
    } catch (...) {
        QMessageBox msgBox;
        msgBox.setText("Can't open file, Unknown error!");
        msgBox.exec();
        return;
    }
    int tabCount = mMainTextTab->count();
    for (int x = tabCount; x > 0; x--) {
        closeTab(x);
    }
    mFilterTextView->setModel(mEmptyItemModel);
    mMainTextView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    mMainTextView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    mMainTextView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    mMainTextView->verticalHeader()->setSectionResizeMode(QHeaderView::Fixed);
    mMainTextView->verticalHeader()->setMinimumSectionSize(sectionSize);
    mMainTextView->verticalHeader()->setMaximumSectionSize(sectionSize);
    mMainTextView->verticalHeader()->setDefaultSectionSize(sectionSize);
    QFileInfo fileInfo(filePath);
    mMainTextTab->setTabText(0, fileInfo.fileName());
    mMainTextTab->setTabToolTip(0, filePath);
}
void MainWindow::filtrMainView(QString text)
{
    mFilterTextViewModel->invalidate();
    data::FilterData::findRows(mMapModels[mMainTextTab->currentIndex() + modelTabShifter],
                               mFilterTextViewModel,
                               text,
                               mTextFilterRegex,
                               mTextFilterCaseSensitive);
    mFilterTextView->setModel(mFilterTextViewModel);
    setViewContetParameters(mFilterTextView);
    mFilterTextView->viewport()->update();
}

void MainWindow::filtrReverseMainView(QString text)
{
    QWidget *newTab = new QWidget(mMainTextTab);
    QTableView *newTabView = new QTableView(newTab);
    newTabView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    newTabView->setShowGrid(false);
    QSortFilterProxyModel *proxyModel = data::FilterData::reverseFindRows(
        mMapModels[mMainTextTab->currentIndex() + modelTabShifter],
        text,
        mReverseTextFilterRegex,
        mReverseTextFilterCaseSensitive);
    setViewDataParameters(newTab, newTabView, proxyModel);
    setViewContetParameters(newTabView);
    QVBoxLayout *widgetLayout = new QVBoxLayout(newTab);
    widgetLayout->addWidget(newTabView);
    newTab->setLayout(widgetLayout);
    mMainTextTab->addTab(newTab, text);
    mMapModels[mMainTextTab->count()] = proxyModel;
}

void MainWindow::setViewDataParameters(QWidget *tab, QTableView *tabView, QAbstractItemModel *model)
{
    tabView->horizontalHeader()->hide();
    tabView->verticalHeader()->hide();
    tabView->setParent(tab);
    tabView->setModel(model);
    tabView->setItemDelegateForColumn(0, &this->mGreyDelegate);
    tabView->setFont(QFont("Times", fontSize));
}

void MainWindow::setViewContetParameters(QTableView *tabView) const
{
    if (tabView == nullptr) {
        return;
    }
    tabView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    tabView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    tabView->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    tabView->verticalHeader()->setMinimumSectionSize(sectionSize);
    tabView->verticalHeader()->setMaximumSectionSize(sectionSize);
    tabView->verticalHeader()->setDefaultSectionSize(sectionSize);
}

void MainWindow::clearMainFilterText([[maybe_unused]] bool status)
{
    mFilterTextView->setModel(mEmptyItemModel);
}

void MainWindow::closeTab(int tab)
{
    if (tab == 0) {
        return;
    }
    mMainTextTab->removeTab(tab);
}

} // namespace widgets