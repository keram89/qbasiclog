find_package(
  Qt5
  COMPONENTS Core Widgets
  REQUIRED)

set(LIB_NAME MainWindow)

set(SOURCES MainWindow.cpp)
set(HEADERS MainWindow.h)

add_library(${LIB_NAME} STATIC ${SOURCES} ${HEADERS})

target_include_directories(${LIB_NAME} INTERFACE .)

set_target_properties(${LIB_NAME} PROPERTIES AUTOMOC ON)

target_link_libraries(${LIB_NAME} PUBLIC Qt5::Widgets)

target_link_libraries(${LIB_NAME} PRIVATE Data)
target_link_libraries(${LIB_NAME} PRIVATE FileOperations)

if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  target_compile_options(${LIB_NAME} PRIVATE ${CMAKE_MSVC_FLAGS})
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  target_compile_options(${LIB_NAME} PRIVATE ${CMAKE_GCC_FLAGS})
elseif(CMAKE_CXX_COMPILER_ID MATCH "Clang")
  target_compile_options(${LIB_NAME} PRIVATE ${CMAKE_CLANG_FLAGS})
endif()
