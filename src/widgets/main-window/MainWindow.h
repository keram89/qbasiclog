#pragma once

#include <QMainWindow>
#include <QMap>
#include <QPainter>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QTabWidget>
#include <QTableView>
#include <QWidget>
#include <qabstractitemmodel.h>
#include <qlist.h>

namespace widgets {

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();

private slots:
    void openFile();

    void filtrMainView(QString text);

    void filtrReverseMainView(QString text);

    void clearMainFilterText(bool status);

    void closeTab(int tab);

private:
    class GrayBackgroundDelegate : public QStyledItemDelegate
    {
    public:
        void paint(QPainter *painter,
                   const QStyleOptionViewItem &option,
                   const QModelIndex &index) const override
        {
            if (index.column() == 0) {
                painter->fillRect(option.rect, QColor(192, 192, 192));
            }
            QStyledItemDelegate::paint(painter, option, index);
        }
    };

    void setViewContetParameters(QTableView *tabView) const;

    void setViewDataParameters(QWidget *tab,
                               QTableView *tabView, QAbstractItemModel *model);

    QWidget *mCentralWidget;
    QTableView *mMainTextView;
    QTableView *mFilterTextView;
    QTabWidget *mMainTextTab;
    QStandardItemModel *mLogsModel;
    QSortFilterProxyModel *mFilterTextViewModel;
    bool mTextFilterRegex;
    bool mTextFilterCaseSensitive;
    QStandardItemModel *mEmptyItemModel;
    bool mReverseTextFilterRegex;
    bool mReverseTextFilterCaseSensitive;
    GrayBackgroundDelegate mGreyDelegate;
    QMap<int, QAbstractItemModel *> mMapModels;

    const int sectionSize = 15;
    const int fontSize = 9;
};

} // namespace widgets
