#include <catch2/catch.hpp>

#include "FilterData.h"

#include <QApplication>
#include <QStandardItemModel>
#include <QTableView>
#include <qsortfilterproxymodel.h>

TEST_CASE("Not existing Model", "[model]")
{
    QStandardItemModel *emptyModel = nullptr;
    QString pattern = "test";
    SECTION("Test filtring non existing model")
    {
        data::FilterData::findRows(emptyModel, nullptr, pattern, false, false);
        SUCCEED("Test done.");
    }
}

TEST_CASE("Filter Empty Model", "[model]")
{
    auto *emptyModel = new QStandardItemModel(0, 2, nullptr);
    QString pattern = "test";

    SECTION("Test filtring one model existing, source")
    {
        data::FilterData::findRows(emptyModel, nullptr, pattern, false, false);
        SUCCEED("Test done.");
    }

    SECTION("Test filtring one model existing, target")
    {
        data::FilterData::findRows(nullptr,
                                   new QSortFilterProxyModel(emptyModel),
                                   pattern,
                                   false,
                                   false);
        SUCCEED("Test done.");
    }
    delete emptyModel;
}
