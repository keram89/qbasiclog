#include <catch2/catch.hpp>

#include "ReverseFilterProxyModel.h"

#include <QStandardItemModel>
#include <qnamespace.h>

TEST_CASE("No exist pattern", "[model]")
{
    auto *model = new data::ReverseFilterProxyModel(nullptr);
    auto *standardModel = new QStandardItemModel(0, 2, nullptr);
    auto *number_item_0 = new QStandardItem("0");
    auto *text_item_0 = new QStandardItem("Onetest");
    auto *number_item_1 = new QStandardItem("1");
    auto *text_item_1 = new QStandardItem("two");
    auto *number_item_2 = new QStandardItem("2");
    auto *text_item_2 = new QStandardItem("toto2!@#$%^&*()");
    auto *number_item_3 = new QStandardItem("3");
    auto *text_item_3 = new QStandardItem("OneTESt");
    standardModel->appendRow({number_item_0, text_item_0});
    standardModel->appendRow({number_item_1, text_item_1});
    standardModel->appendRow({number_item_2, text_item_2});
    standardModel->appendRow({number_item_3, text_item_3});

    model->setSourceModel(standardModel);
    model->setSensitivityMode(Qt::CaseSensitivity::CaseSensitive);
    model->setFilterRole(Qt::UserRole);
    model->setFilterRole(Qt::DisplayRole);
    auto itemModelIndex0 = standardModel->index(0, 1, standardModel->index(0, 1));
    auto itemModelIndex1 = standardModel->index(1, 1, standardModel->index(1, 1));
    auto itemModelIndex2 = standardModel->index(2, 1, standardModel->index(2, 1));
    auto itemModelIndex3 = standardModel->index(3, 1, standardModel->index(3, 1));
    QString pattern = "aaaa";
    SECTION("No regex")
    {
        model->setFilterMode(false);
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == true);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }

    SECTION("Regex")
    {
        model->setFilterMode(true);
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == true);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }

    SECTION("No regex case insensitive")
    {
        model->setSensitivityMode(Qt::CaseSensitivity::CaseInsensitive);
        model->setFilterMode(false);
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == true);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }

    SECTION("Regex case insensitiv")
    {
        model->setSensitivityMode(Qt::CaseSensitivity::CaseInsensitive);
        model->setFilterMode(true);
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == true);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }


    delete model;
    delete standardModel;
}

TEST_CASE("Case insensitivity", "[model]")
{
    auto *model = new data::ReverseFilterProxyModel(nullptr);
    auto *standardModel = new QStandardItemModel(0, 2, nullptr);
    auto *number_item_0 = new QStandardItem("0");
    auto *text_item_0 = new QStandardItem("Onetest");
    auto *number_item_1 = new QStandardItem("1");
    auto *text_item_1 = new QStandardItem("two");
    auto *number_item_2 = new QStandardItem("2");
    auto *text_item_2 = new QStandardItem("toto2!@#$%^&*()");
    auto *number_item_3 = new QStandardItem("3");
    auto *text_item_3 = new QStandardItem("OneTESt");
    standardModel->appendRow({number_item_0, text_item_0});
    standardModel->appendRow({number_item_1, text_item_1});
    standardModel->appendRow({number_item_2, text_item_2});
    standardModel->appendRow({number_item_3, text_item_3});

    model->setSourceModel(standardModel);
    model->setSensitivityMode(Qt::CaseInsensitive);
    model->setFilterRole(Qt::UserRole);
    model->setFilterRole(Qt::DisplayRole);
    auto itemModelIndex0 = standardModel->index(0, 1, standardModel->index(0, 1));
    auto itemModelIndex1 = standardModel->index(1, 1, standardModel->index(1, 1));
    auto itemModelIndex2 = standardModel->index(2, 1, standardModel->index(2, 1));
    auto itemModelIndex3 = standardModel->index(3, 1, standardModel->index(3, 1));

    SECTION("No regex")
    {
        model->setFilterMode(false);
        QString pattern = "test";
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == false);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == false);
    }

    SECTION("Regex")
    {
        model->setFilterMode(true);
        QString pattern = "tes.";
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == false);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == false);
    }

    delete model;
    delete standardModel;
}

TEST_CASE("Case sensitivity", "[model]")
{
    auto *model = new data::ReverseFilterProxyModel(nullptr);
    auto *standardModel = new QStandardItemModel(0, 2, nullptr);
    auto *number_item_0 = new QStandardItem("0");
    auto *text_item_0 = new QStandardItem("Onetest");
    auto *number_item_1 = new QStandardItem("1");
    auto *text_item_1 = new QStandardItem("two");
    auto *number_item_2 = new QStandardItem("2");
    auto *text_item_2 = new QStandardItem("toto2!@#$%^&*()");
    auto *number_item_3 = new QStandardItem("3");
    auto *text_item_3 = new QStandardItem("OneTESt");
    standardModel->appendRow({number_item_0, text_item_0});
    standardModel->appendRow({number_item_1, text_item_1});
    standardModel->appendRow({number_item_2, text_item_2});
    standardModel->appendRow({number_item_3, text_item_3});

    model->setSourceModel(standardModel);
    model->setSensitivityMode(Qt::CaseSensitivity::CaseSensitive);
    model->setFilterRole(Qt::UserRole);
    model->setFilterRole(Qt::DisplayRole);
    auto itemModelIndex0 = standardModel->index(0, 1, standardModel->index(0, 1));
    auto itemModelIndex1 = standardModel->index(1, 1, standardModel->index(1, 1));
    auto itemModelIndex2 = standardModel->index(2, 1, standardModel->index(2, 1));
    auto itemModelIndex3 = standardModel->index(3, 1, standardModel->index(3, 1));

    SECTION("No regex")
    {
        model->setFilterMode(false);
        QString pattern = "test";
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == false);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }

    SECTION("Regex")
    {
        model->setFilterMode(true);
        QString pattern = "tes.";
        model->setPatternToFiltr(pattern);
        REQUIRE(model->filterAcceptsRow(0, itemModelIndex0) == false);
        REQUIRE(model->filterAcceptsRow(1, itemModelIndex1) == true);
        REQUIRE(model->filterAcceptsRow(2, itemModelIndex2) == true);
        REQUIRE(model->filterAcceptsRow(3, itemModelIndex3) == true);
    }

    delete model;
    delete standardModel;
}
