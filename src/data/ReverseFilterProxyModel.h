#pragma once

#include <QSortFilterProxyModel>
#include <qnamespace.h>

namespace data {
class ReverseFilterProxyModel : public QSortFilterProxyModel
{
public:
    explicit ReverseFilterProxyModel(QObject *parent = nullptr)
        : QSortFilterProxyModel(parent)
        , mRegexMode(false)
        , mSensitivityMode(Qt::CaseSensitivity::CaseInsensitive)
    {}

    ~ReverseFilterProxyModel() = default;

    ReverseFilterProxyModel(const ReverseFilterProxyModel&) = delete;

    ReverseFilterProxyModel& operator=(const ReverseFilterProxyModel&) = delete;

    ReverseFilterProxyModel(ReverseFilterProxyModel&&) = delete;

    ReverseFilterProxyModel& operator=(ReverseFilterProxyModel&&) = delete;

    void setFilterMode(bool regexOn) noexcept;

    void setSensitivityMode(Qt::CaseSensitivity mode);

    void setPatternToFiltr(QString &data) noexcept;

    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:
    bool mRegexMode;
    Qt::CaseSensitivity mSensitivityMode;
    QString mPatternToFiltr;
};
} // namespace data