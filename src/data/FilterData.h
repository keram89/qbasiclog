#pragma once

#include <QRegExp>
#include <QSortFilterProxyModel>
#include <QStandardItemModel>
#include <QString>
#include <QVector>

namespace data {
class FilterData
{
public:
    FilterData() = default;

    ~FilterData() = default;

    FilterData(const FilterData&) = delete;

    FilterData& operator=(const FilterData&) = delete;

    FilterData(FilterData&&) = delete;

    FilterData& operator=(FilterData&&) = delete;

    static void findRows(QAbstractItemModel *sourceModel,
                         QSortFilterProxyModel *model,
                         QString &searchString,
                         bool regex,
                         bool caseSensitivity);

    static QSortFilterProxyModel *reverseFindRows(QAbstractItemModel *model,
                                           QString &searchString,
                                           bool regex,
                                           bool caseSensitivity);
};
} // namespace data
