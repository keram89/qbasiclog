#include "ReverseFilterProxyModel.h"
#include <qregexp.h>

namespace data {
bool ReverseFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    QModelIndex sourceIndex = sourceModel()->index(sourceRow, 1, sourceParent);
    QVariant data = sourceIndex.data(filterRole());

    if (data.type() == QVariant::String) {
        QString stringData = data.toString();
        std::string x = data.toString().toStdString();
        std::string xy = mPatternToFiltr.toStdString();
        if (!mRegexMode) {
            if (stringData.contains(mPatternToFiltr, mSensitivityMode)) {
                return false;
            }
        } else {
            if (stringData.contains(QRegExp(mPatternToFiltr, mSensitivityMode))) {
                return false;
            }
        }
        return true;
    }
    return !QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);
}

void ReverseFilterProxyModel::setFilterMode(bool regexOn) noexcept
{
    mRegexMode = regexOn;
}

void ReverseFilterProxyModel::setSensitivityMode(Qt::CaseSensitivity mode)
{
    this->setFilterCaseSensitivity(mode);
    mSensitivityMode = mode;
}

void ReverseFilterProxyModel::setPatternToFiltr(QString &data) noexcept
{
    mPatternToFiltr = data;
}
} // namespace data