#include "FilterData.h"

#include "ReverseFilterProxyModel.h"
#include <qnamespace.h>

namespace data {

void FilterData::findRows(QAbstractItemModel *sourceModel,
                          QSortFilterProxyModel *model,
                          QString &searchString,
                          bool regex,
                          bool caseSensitivity)
{
    if (model == nullptr || sourceModel == nullptr) {
        return;
    }
    model->setSourceModel(sourceModel);
    auto caseSensitivityQt = caseSensitivity ? Qt::CaseSensitive : Qt::CaseInsensitive;
    QRegExp reg(searchString, caseSensitivityQt);
    model->setFilterKeyColumn(1);
    if (regex) {
        model->setFilterRegExp(reg);
    } else {
        model->setFilterCaseSensitivity(caseSensitivityQt);
        model->setFilterFixedString(searchString);
    }
}

QSortFilterProxyModel *FilterData::reverseFindRows(QAbstractItemModel *model,
                                                   QString &searchString,
                                                   bool regex,
                                                   bool caseSensitivity)
{
    if (model == nullptr) {
        return nullptr;
    }
    ReverseFilterProxyModel *proxyModel = new ReverseFilterProxyModel(model);
    proxyModel->setSourceModel(model);
    proxyModel->setFilterRole(Qt::UserRole);
    proxyModel->setFilterRole(Qt::DisplayRole);
    proxyModel->setFilterMode(regex);
    auto caseSensitivityQt = caseSensitivity ? Qt::CaseSensitive : Qt::CaseInsensitive;
    proxyModel->setSensitivityMode(caseSensitivityQt);
    proxyModel->setPatternToFiltr(searchString);
    QRegExp reg(searchString, caseSensitivityQt);
    if (regex) {
        proxyModel->setFilterRegExp(reg);
    } else {
        proxyModel->setFilterCaseSensitivity(caseSensitivityQt);
        proxyModel->setFilterFixedString(searchString);
    }
    return proxyModel;
}
} // namespace data