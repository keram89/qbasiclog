#pragma once

#include "ILoadData.h"
#include <qt5/QtGui/QStandardItemModel>

namespace fileoperations {

class LoadDataSingleChunk : public ILoadData
{
public:
    LoadDataSingleChunk() = default;

    ~LoadDataSingleChunk() = default;

    void loadDataToModel(QStandardItemModel *model, QString &pathToFile) override;
};
} // namespace data
