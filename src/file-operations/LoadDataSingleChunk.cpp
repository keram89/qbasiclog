#include "LoadDataSingleChunk.h"

#include <filesystem>
#include <QFile>
#include <QTextStream>

namespace fileoperations {
void LoadDataSingleChunk::loadDataToModel(QStandardItemModel *model, QString &pathToFile)
{
    if (model == nullptr) {
        return;
    }
    QFile file(pathToFile);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);
        QList<QStandardItem *> row_items;
        quint64 counter = 0;
        while (!in.atEnd()) {
            QStandardItem *index = new QStandardItem(QString::number(counter));
            QStandardItem *textItem = new QStandardItem(in.readLine());
            row_items.clear();
            row_items << index << textItem;
            model->appendRow(row_items);
            counter++;
        }
        file.close();
    } else {
        throw std::filesystem::filesystem_error(std::string("File can't be open:"
                                                            + pathToFile.toStdString()),
                                                std::make_error_code(
                                                    std::errc::no_such_file_or_directory));
    }
}
} // namespace data