#pragma once

#include <QStandardItemModel>

namespace fileoperations {
class ILoadData
{
public:
    ~ILoadData() = default;

    virtual void loadDataToModel(QStandardItemModel *model, QString &pathToFile) = 0;
};
} // namespace fileoperations